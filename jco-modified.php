<?php

/*
Plugin Name: Last Modified
Plugin URI: http://www.jco.fi
Version: 0.2.2
Author: JCO
Author URI: http://www.jco.fi
Text Domain: jco-modified
Domain Path: /languages
Description: Plugin that shows the last modified posts as a dashboard widget.

------------------------------------------------------------------------
Copyright 2016 JCO Digital

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses.
*/

namespace JCOModified;

add_action( 'wp_dashboard_setup', 'JCOModified\dashboard_init' );

function dashboard_init() {
	date_default_timezone_set('Europe/Helsinki');
	wp_add_dashboard_widget(
		'jco_last_modified', // Widget slug.
		'Last modified', // Title.
		'JCOModified\last_modified' // Display function.
	);
}

function last_modified() {
	// Query Arguments
	$args = array(
		'post_type'           => 'any',
		'orderby'             => 'modified',
		'ignore_sticky_posts' => '1',
	);

	$string = '<table>';
	foreach ( get_posts( $args ) as $post_orig ) {
		$revs = wp_get_post_revisions( $post_orig->ID );
		if (empty($revs)) {
			$post = $post_orig;
		} else {
			$post = array_shift($revs);
		}

		$dateTime = strtotime($post->post_modified);
		$age = time() - $dateTime;
		if ($age > 1209600) {
			// Older than two weeks
			$age_text = date('d.m.Y',$dateTime);
		} elseif ($age > 86400) {
			// Older than one day
			$age_text = round($age / 86400). '&#160;'. __('days ago');
		} elseif ($age > 3600) {
			// Older than an hour
			$age_text = round($age / 3600). '&#160;'. __('hours ago');
		} else {
			// Less than an hour
			$age_text = round($age / 60). '&#160;'. __('minutes ago');
		}
		$author = get_userdata($post->post_author);

		$string .= '<tr>';
		$string .= '<td>' . __($post_orig->post_type) . '</td>';
		$string .= '<td><a href="post.php?post=' . $post->ID . '&action=edit" title="' . $post->post_title . '">' . truncate($post->post_title) . '</a></td>';
		$string .= '<td>' . $author->display_name . '</td>';
		$string .= '<td>' . $age_text . '</td>';
		$string .= '</tr>';
	}
	$string .= '</table>';
	echo $string;
}

function truncate ($string, $len = 25) {
	if (mb_strlen($string) > $len) {
		return mb_substr($string,0,$len-2) . '...';
	}
	return $string;
}
